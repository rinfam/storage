import { URL } from "url";
import { Readable } from "stream";

export interface Storage {
  type: "storage";
  write: (url: URL, stream: Readable) => Promise<void>;
  read: (url: URL) => Promise<Readable>;
}
