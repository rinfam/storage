## [1.0.1](https://gitlab.com/rinfam/storage/compare/v1.0.0...v1.0.1) (2019-12-01)


### Bug Fixes

* add repository and publishConfig ([60baaab](https://gitlab.com/rinfam/storage/commit/60baaaba442f58d20c988ae18d22e20a26bea005))

# 1.0.0 (2019-12-01)


### Features

* initial commit ([e44c9c4](https://gitlab.com/rinfam/storage/commit/e44c9c4a733e3f5352056c81c1cbcdd56434ebf1))
